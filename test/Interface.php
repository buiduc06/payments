<?php
namespace Test;

interface PaymentInterface
{
    public function paymentProcess();
}
