<?php
namespace Test;

class Product
{
    public function buyProduct(PaymentInterface $paymentType)
    {
        $paymentType->paymentProcess();
    }
}
